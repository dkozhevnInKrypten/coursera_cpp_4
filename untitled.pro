QMAKE_EXTRA_TARGETS += before_build makefilehook

makefilehook.target = $(MAKEFILE)
makefilehook.depends = .beforebuild

PRE_TARGETDEPS += .beforebuild

before_build.target = .beforebuild
before_build.depends = FORCE
before_build.commands = chcp 1251

TEMPLATE = app
CONFIG += console c++17 warn_on
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std:c++17
QMAKE_CXXFLAGS += -O2
#QMAKE_CXXFLAGS += /Wfatal-errors

SOURCES += \
    main.cpp \
    main.cpp

HEADERS += \
    booking.h \
    new_booking_providers.h \
    new_trip_manager.h \
    old_booking_providers.h \
    old_trip_manager.h \
    profile.h \
    test_runner.h



#isEmpty(TARGET_EXT) {
#    win32 {
#        TARGET_CUSTOM_EXT = .exe
#    }
#    macx {
#        TARGET_CUSTOM_EXT = .app
#    }
#} else {
#    TARGET_CUSTOM_EXT = $${TARGET_EXT}
#}

#win32 {
#    DEPLOY_COMMAND = windeployqt
#}
#macx {
#    DEPLOY_COMMAND = macdeployqt
#}

#CONFIG( debug, debug|release ) {
#    # debug
#    DEPLOY_TARGET = $$shell_quote($$shell_path($${OUT_PWD}/debug/$${TARGET}$${TARGET_CUSTOM_EXT}))
#} else {
#    # release
#    DEPLOY_TARGET = $$shell_quote($$shell_path($${OUT_PWD}/release/$${TARGET}$${TARGET_CUSTOM_EXT}))
#}

#  # Uncomment the following line to help debug the deploy command when running qmake
#  warning($${DEPLOY_COMMAND} $${DEPLOY_TARGET})

# Use += instead of = if you use multiple QMAKE_POST_LINKs
#QMAKE_POST_LINK = $${DEPLOY_COMMAND} $${DEPLOY_TARGET}
