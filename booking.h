#pragma once

namespace RAII{

template<typename T>
class Booking{
public:
    Booking(T* provider, int id):provider_(provider), id_(id){};
    //Booking()= delete;

    Booking(const Booking&) = delete;
    Booking(Booking&& other){
        provider_=other.provider_;
        id_=other.id_;
        other.provider_=nullptr;
    };

    Booking& operator=(const Booking&) = delete;
    Booking& operator=(Booking&&other){
        provider_=other.provider_;
        id_=other.id_;
        other.provider_=nullptr;
        return *this;
    };

    ~Booking(){
        if (provider_ != nullptr){
            provider_->CancelOrComplete(*this);
        }
    }


    T* provider_;
    int id_;
};

}
